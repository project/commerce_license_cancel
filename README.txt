This it the abbreviated documentation for http://www.drupal.org/project/commerce_license_cancel.

FULL DOCUMENTATION: http://www.drupal.org/node/2484995.

This module provides a few ways for users to cancel Commerce License licenses.